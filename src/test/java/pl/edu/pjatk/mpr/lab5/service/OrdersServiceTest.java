package pl.edu.pjatk.mpr.lab5.service;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import pl.edu.pjatk.mpr.lab5.model.Address;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

public class OrdersServiceTest {

    private static ClientDetails CLIENT1 = new ClientDetails("stanaz", "Stanisław", "Nazwiskowy", 34);
    private static ClientDetails CLIENT2 = new ClientDetails("stefcio", "Stefan", "Iksiński", 6);
    private static ClientDetails CLIENT3 = new ClientDetails("kowal", "Jan", "Kowalski", 19);

    private static Address ADDRESS1 = new Address("polandia", "miasto", "123-xx", "uliczna", "2312", "1234");
    private static Address ADDRESS2 = new Address("niepolandia", "niemiasto", "999-66", "nieuliczna", "11", "1");
    private static Address ADDRESS3 = new Address("Polska", "Warszawa", "111-11", "Al. Jerozolimskie", "1", "1");

    private static OrderItem ORDER_ITEM1 = new OrderItem("nazwa", "opis przedmiotu", 2.99);
    private static OrderItem ORDER_ITEM2 = new OrderItem("kolejny produkt", "opis kolejnego przedmiotu", 10);
    private static OrderItem ORDER_ITEM3 = new OrderItem("produkt nr 3", "opis nr 3", 7);
    private static OrderItem ORDER_ITEM4 = new OrderItem("produkt nr 4", "opis nr 4", 99);
    private static OrderItem ORDER_ITEM5 = new OrderItem("produkt nr 5", "opis nr 5", 1);
    private static OrderItem ORDER_ITEM6 = new OrderItem();

    private static List <OrderItem> ITEMS1 = Arrays.asList(ORDER_ITEM1,ORDER_ITEM2,ORDER_ITEM3,ORDER_ITEM4,ORDER_ITEM5,ORDER_ITEM6);
    private static List <OrderItem> ITEMS2 = Arrays.asList(ORDER_ITEM1,ORDER_ITEM2,ORDER_ITEM2,ORDER_ITEM6);
    private static List <OrderItem> ITEMS3 = Arrays.asList(ORDER_ITEM2);
    private static List <OrderItem> ITEMS4 = Arrays.asList(ORDER_ITEM1,ORDER_ITEM1);

    private static Order ORDER1 = new Order(CLIENT1,ADDRESS1,ITEMS1,"A comment");
    private static Order ORDER2 = new Order(CLIENT2,ADDRESS2,ITEMS2,"comment22");
    private static Order ORDER3 = new Order(CLIENT3,ADDRESS1,ITEMS3,"comment333");
    private static Order ORDER4 = new Order(CLIENT3,ADDRESS1,ITEMS4,"last comment");

    private static List<Order> ORDERS1 = Arrays.asList(ORDER3);
    private static List<Order> ORDERS2 = Arrays.asList(ORDER2);
    private static List<Order> ALL_ORDERS = Arrays.asList(ORDER1, ORDER2, ORDER3);

    @Test
    public void getAndReturnNull_findOrdersWhichHaveMoreThan5OrderItems() {
        List<Order> orders = null;
        assertThat(OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders)).isNull();
    }

    @Test
    public void getAndReturnEmpty_findOrdersWhichHaveMoreThan5OrderItems() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders)).isEmpty();
    }

    @Test
    public void getListAndReturnEmpty_findOrdersWhichHaveMoreThan5OrderItems() {
        List<Order> orders = ORDERS1;
        assertThat(OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders)).isEmpty();
    }


    @Test
    public void getAndReturnNull_findOldestClientAmongThoseWhoMadeOrders() {
        List<Order> orders = null;
        assertThat(OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders)).isNull();
    }

    @Test
    public void getAndReturnEmpty_findOldestClientAmongThoseWhoMadeOrders() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders)).isNull();
    }

    @Test
    public void getValidReturn_findOldestClientAmongThoseWhoMadeOrders() {
        List<Order> orders = ALL_ORDERS;
        assertThat(OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders)).isEqualTo(CLIENT1);
    }


    @Test
    public void getAndReturnNull_findOrderWithLongestComments() {
        List<Order> orders = null;
        assertThat(OrdersService.findOrderWithLongestComments(orders)).isNull();
    }

    @Test
    public void getAndEmptyReturnNull_findOrderWithLongestComments() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.findOrderWithLongestComments(orders)).isNull();
    }

    @Test
    public void getValidReturn_findOrderWithLongestComments() {
        List<Order> orders = ALL_ORDERS;
        assertThat(OrdersService.findOrderWithLongestComments(orders)).isEqualTo(ORDER3);
    }


    @Test
    public void getAndReturnNull_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
        List<Order> orders = null;
        assertThat(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders)).isNull();
    }

    @Test
    public void getAndReturnEmpty_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders)).isEmpty();
    }

    @Test
    public void getListAndReturnEmpty_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
        List<Order> orders = ORDERS2;
        assertThat(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders)).isEmpty();
    }


    @Test
    public void getAndReturnNull_getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
        List<Order> orders = null;
        assertThat(OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders)).isNull();
    }

    @Test
    public void getAndReturnEmpty_getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders)).isEmpty();
    }

    @Test
    public void getListAndReturnEmpty_getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
        List<Order> orders = ORDERS2;
        assertThat(OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders)).isEmpty();
    }


    @Test
    public void getAndReturnNull_groupOrdersByClient() {
        List<Order> orders = null;
        assertThat(OrdersService.groupOrdersByClient(orders)).isNull();
    }

    @Test
    public void getAndReturnEmpty_groupOrdersByClient() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.groupOrdersByClient(orders)).isEmpty();
    }


    @Test
    public void getAndReturnNull_partitionClientsByUnderAndOver18() {
        List<Order> orders = null;
        assertThat(OrdersService.partitionClientsByUnderAndOver18(orders)).isNull();
    }

    @Test
    public void getEmptyReturnNull_partitionClientsByUnderAndOver18() {
        List<Order> orders = new ArrayList<>();
        assertThat(OrdersService.partitionClientsByUnderAndOver18(orders)).isNull();
    }

    @Test
    public void getAndReturnCollection_partitionClientsByUnderAndOver18() {
        List<Order> orders = ORDERS2;
        assertThat(OrdersService.partitionClientsByUnderAndOver18(orders)).hasSize(2);
    }
}