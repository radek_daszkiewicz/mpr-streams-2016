package pl.edu.pjatk.mpr.lab5.service;


import java.util.Comparator;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class OrdersService {
    
    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
        if(orders == null) return null;
        List <Order> result = orders.stream()
                .filter(o -> o.getItems().size() > 5)
                .collect(Collectors.toList());
        return result;
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
        if(orders == null || orders.isEmpty()) return null;
        ClientDetails result = orders.stream()
                .map(Order::getClientDetails)
                .max((c,c2) -> (c.getAge() - c2.getAge()))
                .get();
        if(result == null) return null;
        return result;
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
        if(orders == null || orders.isEmpty()) return null;
        Order result = orders.stream()
                .max((o,o2) -> (o.getComments().length() - o2.getComments().length()))
                .get();
        if(result == null) return null;
        return result;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
        if(orders == null) return null;
        String result = orders.stream()
                .map(o -> o.getClientDetails())
                .filter(cd -> cd.getAge() > 18)
                .map(cd -> (cd.getName() + " " + cd.getSurname()))
                .collect(Collectors.joining(","));
        return result;
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
        if(orders == null) return null;
        List<String> result;
        result = orders.stream()
                .filter(o -> o.getComments().startsWith("A"))
                .flatMap(o -> o.getItems().stream())
                .map(i -> i.getName())
                .sorted((i1, i2) -> i1.compareTo(i2))
                .collect(Collectors.toList());
        return result;
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
        
        orders.stream()
                .filter(o -> o.getClientDetails().getName().startsWith("S"))
                .map(o -> o.getClientDetails().getLogin().toUpperCase())
                .forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
        if(orders == null) return null;
        return orders.stream()
                        .collect(Collectors.groupingBy(o -> o.getClientDetails()));  
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        if(orders == null || orders.isEmpty()) return null;
        Map<Boolean, List<ClientDetails>> isAdult;
        isAdult = orders.stream()
                        .map(o -> o.getClientDetails())
                        .collect(Collectors.partitioningBy(c -> c.getAge() >= 18));
        return isAdult;
    }

}
